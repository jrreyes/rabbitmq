# README

This app show 2 examples for use rabbit. The first app is a normal 
message broker communication, and the second one is for segment multiples messages between N brokers.


Global Steps:

-> bundle install


=====  FIRST EXAMPLE  =====

Open 2 windows terminal and in both open:
-> rails c

After in the first windows, we will open the receiver:
    -> re = ReceiveService.new()
    -> re.first_example()
   With this command the receiver start to listen the sender,
   and we will see:
   -> [*] Waiting for messages. To exit press CTRL+C


After in the second windows, we will open the sender:
    -> se = SendService.new()
    -> se.first_example()
    
    When we send the message we will see the next:
    -> [x] Sent 'Hello World!'
        => true
         
    And in the Receiver Window we will see this:
    -> [x] Received Hello World!
    
    
=====  SECOND EXAMPLE  =====
    
Here we will open 3 windows and in all the windows open:
-> rails c

First window and Second window, one receiver in every terminal:
    -> re = ReceiveService.new()
    -> re.second_example()

In the thirth window, we will send messages:
    -> se = SendService.new()
    -> se.second_example()
    If you make this many times you will see how between the receiver
    they are taking the task in different times, while one receiver is working.
    
    
    



