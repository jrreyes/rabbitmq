require 'bunny'

class ReceiveService
  def initialize

  end

  def first_example
    connection = Bunny.new(automatically_recover: false)
    connection.start

    channel = connection.create_channel
    queue = channel.queue('hello')

    begin
      puts ' [*] Waiting for messages. To exit press CTRL+C'
      queue.subscribe(block: true) do |_delivery_info, _properties, body|
        puts " [x] Received #{body}"
      end
    rescue Interrupt => _
      connection.close

      exit(0)
    end
  end

  def second_example
    connection = Bunny.new(automatically_recover: false)
    connection.start

    channel = connection.create_channel
    queue = channel.queue('task_queue', durable: true)

    channel.prefetch(1)
    puts ' [*] Waiting for messages. To exit press CTRL+C'

    begin
      queue.subscribe(manual_ack: true, block: true) do |delivery_info, _properties, body|
        puts " [x] Received '#{body}'"
        # imitate some work
        sleep body.count('.').to_i
        puts ' [x] Done'
        channel.ack(delivery_info.delivery_tag)
      end
    rescue Interrupt => _
      connection.close
    end
  end
end