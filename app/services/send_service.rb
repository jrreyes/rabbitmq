require 'bunny'

class SendService

  def initialize

  end

  def first_example
    connection = Bunny.new(automatically_recover: false)
    connection.start

    channel = connection.create_channel
    queue = channel.queue('hello')

    channel.default_exchange.publish('Hello World!', routing_key: queue.name)
    puts " [x] Sent 'Hello World!'"

    connection.close
  end

  def second_example
    connection = Bunny.new(automatically_recover: false)
    connection.start

    channel = connection.create_channel
    queue = channel.queue('task_queue', durable: true)

    message = ARGV.empty? ? 'Hello World!' : ARGV.join(' ')

    queue.publish(message, persistent: true)
    puts " [x] Sent #{message}"

    connection.close
  end
end